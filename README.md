# Ansible - Deploy services - KUMAR Aman & Théo DILLENSEGER

## Description
Ce projet a pour but de déployer des services sur des machines virtuelles. Il utilise Ansible pour déployer les services et Vagrant pour créer les machines virtuelles.

## Prérequis
- Vagrant
- Ansible
- VirtualBox

## Utilisation
- Lancer la commande `vagrant up` pour créer les machines virtuelles
- Lancer la commande `ansible-playbook -i inventories/openstack.yml playbook/install.yml` pour déployer les services sur les machines virtuelles

## Commandes vagrant
`vagrant up`: monter toutes les VM définies dans le `Vagrantfile`.\
`vagrant destroy`: détruire toutes les VM définies dans le `Vagrantfile`.\
`vagrant up <args>`: monter la ou les VMs spécifiées.
`vagrant destroy <args>`: détruire la ou les VMs spécifiées.
